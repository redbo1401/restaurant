package com.afpa.restaurant.model;

import java.util.List;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Commande {
	@Id
	private int id;
//	@OneToMany
//	List<Produit> produits;
	@Column
	private double prixTotal;
	@Column
	private String remarque;

}
