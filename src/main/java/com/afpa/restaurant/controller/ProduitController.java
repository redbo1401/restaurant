package com.afpa.restaurant.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.afpa.restaurant.dao.IDaoProduit;
import com.afpa.restaurant.model.Produit;

@RestController
public class ProduitController {

	@Autowired
	IDaoProduit daop;
	
	@GetMapping(path = {"/produit/{id}"})
	public ResponseEntity<Produit> getProduitById (@PathVariable int id){
		return new ResponseEntity<Produit>(this.daop.findById(id).get(), HttpStatus.OK);
	}
	
	@GetMapping(path = {"/produits"})
	public ResponseEntity<List<Produit>> getAllProduits (){
		return new ResponseEntity<List<Produit>>(this.daop.findAll(), HttpStatus.OK);
	}

}
