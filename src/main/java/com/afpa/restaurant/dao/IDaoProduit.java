package com.afpa.restaurant.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.afpa.restaurant.model.Produit;



@Repository
public interface IDaoProduit extends CrudRepository<Produit,Integer>{
	//2eme paramétre correspond toujours à l'id, et le 1er la classe concernée
	
	public List<Produit> findAll();

}
