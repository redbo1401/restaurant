//maVar est de type String
var maVar = 'toto';
console.log(typeof maVar);

console.log('----------------')
//maVar est de type Object
maVar = {nom:'salim'};
console.log(typeof maVar);
console.log(maVar.nom);

console.log('----------------')
//maVar est de type function
maVar = (param) => {
    return 'Bonjour ' + param;
}

console.log( maVar );
console.log( maVar('redouane') );

console.log('----------------')
function maFunction(param) {
    return 'Bonjour ' + param;
  }

var leRetour = maFunction('moustapha');
console.log(leRetour);

//-----------------------------------------------
console.log('*********************************');
//je declare ma fonction ret
ret = (param) => {
    var nom;
            setTimeout((param) => {
              nom = 'redouane';
              console.log('***************' + 'Bonjour ' + nom );
            }, 3000);
    return '++++++++++++++' + param + ' '+ nom;
}

//je declare ma fonction test
function test(param) {
    let hello = ret(param);

    console.log(hello);
    console.log('------------> ' +param);

}

//j'execute ma fonction test 
test('Hello');